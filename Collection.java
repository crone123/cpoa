import java.util.*;
import java.lang.*;
public class Collection
{
	ArrayList<Produit> arrProduit;
	ArrayList<TVA> arrTVA;
	ArrayList<Produit_Type> arrProduitType;
	public Collection()
	{
		arrProduit = new ArrayList<Produit>();
		arrTVA = new ArrayList<TVA>();
		arrProduitType = new ArrayList<Produit_Type>();
	}
	public void addProduit(Produit p)
	{
		arrProduit.add(p);
	}
	public void addTVA(TVA t)
	{
		arrTVA.add(t);
	}
	public void addPType(PType pt)
	{
		arrPType.add(pt);
	}
	public void delProduit(int n)
	{
		arrProduit.remove(n);	
	}
	public void delTVA(int n)
	{
		arrTVA.remove(n);
	}
	public void delPType(int n)
	{
		arrPType.remove(n);
	}
	public int getSizeTVA()
	{
		return arrTVA.size();
	}
	public int getSizeProduit()
	{
		return arrProduit.size();
	}
	public int getSizePType()
	{
		return arrPType.size();
	}
	public Produit getProduit(int n)
	{
		return arrProduit.get(n);
	}
	public TVA getTVA(int n)
	{
		return arrTVA.get(n);
	}
	public PType getPType(int n)
	{
		return arrPType.get(n);
	}
}
