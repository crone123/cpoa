public class LigneProduit()
{
	private int idf;
	private int idp;
	private int qte;
	
	public LigneProduit(int idf, int idp, int qte)
	{
		this.setIdf(idf);
		this.setIdp(idp);
		this.setQte(qte);
	}
	
	public int getIdf()
	{
		return this.idf;
	}
	
	public void setIdf(int idf)
	{
		this.idf=idf;
	}
	
	public int getIdp()
	{
		return this.idp;
	}
	
	public void setIdp(int idp)
	{
		this.idp=idp;
	}
	
	public int getQte()
	{
		return this.qte;
	}
	
	public void setQte(int qte)
	{
		this.qte=qte;
	}
	
	public String toString()
	{
		return "" + "idf=" + idf + "," + "idp=" + idp + "," + "qte=" + qte + "," + ".";
	}
}
