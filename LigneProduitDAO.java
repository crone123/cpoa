import java.util.*;
import java.sql.*;
public class LigneProduitDAO implements DAO<ArrayList<LigneProduit>>
{
	public ArrayList<LigneProduit> getById(int n)
	{
		ArrayList<LigneProduit> a = null;
		try
		{
			Connection laConnexion = ConnexionManager.getConnexion().getConnection();
			PreparedStatement requete = laConnexion.prepareStatement("select * from ligne_facture id_facture=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			a = new ArrayList<LigneProduit>();
			LigneProduit l = null;
			while (res.next())
			{
				l = new LigneProduit();
				l.setIdf(res.getInt(1));
				l.setIdp(res.getInt(2));
				l.setQte(res.getInt(3));
				a.add(l);
			}
		}
		catch(SQLException e)
		{
			System.out.println("Erreur: " + e.getMessage());
		}
		return a;
	}
}
