import java.sql.*;

public class TvaDAO implements DAO<TVA>
{
	@Override
	public TVA getById(int id)
	{
		TVA t = null;
		try
		{
			Connection laConnexion = ConnexionManager::getConnexion().getConnection();
			PreparedStatement requete = laConnexion.prepareStatement("select * from tva where id =?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			t = new TVA();
			while (res.next())
			{
				t.setId(res.getInt(1));
				t.setLib(res.getString(2));
			}	
		}
		catch(SQLException e)
		{
			System.out.println("Erreur: " + e.getMessage());
		}
		return t;
	}
}
