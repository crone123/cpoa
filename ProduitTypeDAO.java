import java.sql.*;

public class ProduitTypeDAO implements DAO<ProduitType>
{
	@Override
	public ProduitType getById(int id)
	{
		ProduitType pt = null;
		try
		{
			Connection laConnexion = ConnexionManager::getConnexion().getConnection();
			PreparedStatement requete = laConnexion.prepareStatement("select * from produit_type where id =?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			pt = new ProduitType();
			while (res.next())
			{
				pt.setId(res.getInt(1));
				pt.setLib(res.getString(2));
			}
		}
		catch(SQLException e)
		{
			System.out.println("Erreur: " + e.getMessage());
		}
		return pt;
	}
} 
