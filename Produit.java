import java.util.*;
import java.lang.*;

public class Produit
{
	private int ref;
	private String lib;
	private float prix;
	public Produit()
	{
		this("");
	}
	public Produit(String lib)
	{
		this(-1, lib, 0);
	}
	
	public Produit(int ref, String lib, float prix)
	{
		this.setRef(ref);
		this.setLib(lib);
		this.setPrix(prix);
	}
	
	public void setRef(int ref)
	{
		this.ref=ref;
	}
	
	public int getRef()
	{
		return this.ref;
	}
	
	public void setLib(String lib)
	{
		this.lib=lib;
	}
	
	public String getLib()
	{
		return new String(this.lib);
	}
	
	public void setPrix(float prix)
	{
		this.prix=prix;
	}
	
	public float getPrix()
	{
		return this.prix;
	}
	
	public String toString()
	{
		return "(" + (this.ref>=0?this.ref:"Nouvelle référence") + ") " + this.lib + " "+ this.prix;
	}
}
