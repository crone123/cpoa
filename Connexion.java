import java.util.*;
import java.lang.*;
import java.sql.*;

public class Connexion
{
	private Connection connexion;
	public Connexion()
	{
		//connexion = creeConnexion();
	}
	public boolean creeConnexion()
	{
		boolean r = false;
		//String url = "jdbc:mysql://127.0.0.1:10000/crone1u_cpoa";
		//String login = "crone1u_appli";
		//String pwd = "31512635";
		String url = "jdbc:mysql://127.0.0.1:10000/cpoa_td2";
		String login = "deviut";
		String pwd = "prog";
		Connection maConnexion = null;
		try
		{
			maConnexion = DriverManager.getConnection(url, login, pwd);
			System.out.println("Connexion réussie !");
			r = true;
		}
		catch (SQLException sqle)
		{
			System.out.println("Erreur de connexion: " + sqle.getMessage());
		}
		connexion = maConnexion;
		return r;
	}
	
	public Connection getConnection()
	{
		return connexion;
	}
	public ResultSet exec(String str)
	{
		ResultSet res = null;
		try
		{
			Statement r = connexion.createStatement();
			res = r.executeQuery(str);
			System.out.println("Exec: OK");
		}
		catch (SQLException sqle)
		{
			System.out.println("Erreur d'execution de requête: " + sqle.getMessage());
		}
		return res;
	}
}


