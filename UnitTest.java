import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class UnitTest
{
	@Test
	public void test1()
	{
		Unit t = new Unit();
		assertEquals(24, t.fonction1());
	}	
	
	
	@Test
	public void test2()
	{
		Unit t = new Unit();
		assertEquals(21, t.fonction2(7,3));
	}
	
	@Test
	public void test3()
	{
		Unit t = new Unit();
		assertEquals(10, t.fonction3(7,3));
	}
}
