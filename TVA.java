import java.util.*;
import java.lang.*;

public class TVA
{
	private float tva;
	private String lib;
	public TVA()
	{
		tva = 0;
		lib = new String();
	}
	public TVA(String lib)
	{
		this(-1, lib);
	}
	public TVA(float tva, String lib)
	{
		this.setTVA(tva);
		this.setLib(lib);
	}
	public float getTVA()
	{
		return this.tva;
	}
	public void setTVA(float a)
	{
		tva = a;
	}
	public String getLib()
	{
		return this.lib;
	}
	public void setLib(String s)
	{
		if(nom == null || s.trim().lenght() == 0)
		{
			throw new IllegalArgumentException("Table vide !");
		}
		lib = s;
	}
	public String toString()
	{
		return "(" + (this.tva >= 0 ? this.tva:"Nouveau") + ")" + this.lib;
	}
}
