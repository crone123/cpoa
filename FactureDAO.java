import java.sql.*;

public class FactureDAO implements DAO<Facture>
{
	@Override
	public Facture getById(int id)
	{
		Facture f = null;
		try
		{
			Connection laConnexion = ConnexionManager.getConnexion().getConnection();
			PreparedStatement requete = laConnexion.prepareStatement("select * from facture where id =?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			f = new Facture();
			while (res.next())
			{
				f.setId(res.getInt(1));
				f.setDate(res.getString(2));
			}	
		}
		catch(SQLException e)
		{
			System.out.println("Erreur: " + e.getMessage());
		}
		return f;
	}
}
