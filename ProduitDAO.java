import java.sql.*;

public class ProduitDAO implements DAO<Produit>
{
	public ProduitDAO()
	{
	}
	@Override
	public Produit getById(int id)
	{
		Produit p = null;
		try
		{
			Connection laConnexion = ConnexionManager.getConnexion().getConnection();
			PreparedStatement requete = laConnexion.prepareStatement("select * from produit where id =?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			p = new Produit();
			while (res.next())
			{
				p.setRef(res.getInt(1));
				p.setLib(res.getString(2));
				p.setPrix(res.getFloat(3));
			}	
		}
		catch(SQLException e)
		{
			System.out.println("Erreur: " + e.getMessage());
		}
		return p;
	}
}
