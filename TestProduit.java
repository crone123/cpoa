import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestProduit
{
	Produit p;
	public TestProduit()
	{
		p = new Produit();
	}
	@Test
	public void testRef()
	{
		p.setRef(12);
		assertEquals(12, p.getRef());
	}
	
	@Test
	public void testLib()
	{
		p.setLib("lib");
		assertEquals(true, p.getLib().equals(new String("lib")));
	}
	
	@Test
	public void testPrix()
	{
		p.setPrix(150);
		assertEquals(150, p.getPrix(), 0);
	}
}
