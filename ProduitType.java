import java.util.*;
import java.lang.*;

public class ProduitType
{
	private int produitId;
	private String lib;
	public ProduitType()
	{
		produiId = 0;
		lib = new String();
	}
	public void setId(int t)
	{
		produitId = t;
	}
	public int getId()
	{
		return produitId;
	}
	public String getLib()
	{
		return new String(lib);
	}
	public void setLib(String l)
	{
		lib = new String(l);
	}
}
