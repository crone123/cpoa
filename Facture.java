import java.util.*;

public class Facture 
{
	
	private int id;
	private String date;
	private ArrayList<LigneFacture> lignes;
	public Facture(String date)
	{
		this(-1, date);
	}
	
	public Facture(int id, String date)
	{
		this.setId(id);
		this.setDate(date);
		lignes = new ArrayList<LigneFacture>();
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public String getDate()
	{
		return this.date;
	}
	
	public void setDate(String date)
	{
		this.date=date;
	}
	public int ligneSize()
	{
		return lignes.size();
	}
	public void addLigne(LigneFacture l)
	{
		lignes.add(l);
	}
	public void removeLigne(int n)
	{
		lignes.remove(n);
	}
	public LigneFacture getLigne(int n)
	{
		return ligne.get(n);
	}
	public String toString()
	{
		return "(" + (this.id>=0?this.id:"Nouvelle facture") + ")" + this.date;
	}
}
