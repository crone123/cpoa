import java.sql.*;
public class ConnexionManager
{
	static boolean created = false;
	static Connexion con = null;
	public static Connexion getConnexion()
	{
		
		if(!created)
		{
			created = true;
			con = new Connexion();
			con.creeConnexion();
		}
		return con;
	}
}


//Connection co = ConnexionManager::getConnexion().getConnection();
